/*************************************************************************
	> File Name: homework_2.c // 字符串位置
	> Author: Jangwee
	> Mail: jangwee11211039@gmail.com 
	> Created Time: 2014年03月29日 星期六 22时58分24秒
 ************************************************************************/

#include<stdio.h>
#include<string.h>

int string_pos(char* ori_str,int ori_str_length,char* find_str,int find_str_length)
{
	int pori_str=0;
	int pfind_str=0;
//	while(*(ori_str+pori_str)!='\0')
	while(pori_str<(ori_str_length-1))
	{
		while(*(ori_str+pori_str)!=*(find_str))
		{
			++pori_str;
			if(pori_str==(ori_str_length-1))
			{
				return -1;
			}
		}
		int note_pori_str=pori_str;
//		while(*(find_str+pfind_str)!='\0')
		while(pfind_str<find_str_length)
		{
			if(*(ori_str+note_pori_str)!=*(find_str+pfind_str))
				break;
			++note_pori_str;
			++pfind_str;
		}
		if(pfind_str==find_str_length)
		{
			return pori_str;
		}
		pfind_str=0;
		printf("......");
		++pori_str;
	}
	return -1;
}


int main()
{
	char pori_str_[1024];
	char find_str_[1024];
	printf("please input origin string : \n");
	scanf("%s",pori_str_);
	printf("please input find string : \n");
	scanf("%s",find_str_);
	int length_ori_=strlen(pori_str_);
	int length_find_=strlen(find_str_);
    int reval=string_pos(pori_str_,length_ori_,find_str_,length_find_);
	if(reval<0)
		printf("sorry,not find...\n");
	else
		printf("congratulation! postion is : %d\n",reval+1);
	return 0;
}
