/*************************************************************************
	> File Name: homework_8.c  逆序对问题
	> Author: Jangwee
	> Mail: jangwee11211039@gmail.com 
	> Created Time: 2014年03月30日 星期日 20时22分59秒
 ************************************************************************/

#include<stdio.h>
int reverse_order_pairs(int*,int,int);
int merge_pairs(int*,int,int,int);



int reverse_order_pairs(int* iDatas,int iLow,int iHigh)
{
	if(iLow==iHigh)
		return 0;
	if(iLow<iHigh)
	{
		int c_1,c_2,c_3;
		int iMid=(iLow+iHigh)/2;
		c_1=reverse_order_pairs(iDatas,iLow,iMid);
		c_2=reverse_order_pairs(iDatas,iMid+1,iHigh);
		c_3=merge_pairs(iDatas,iLow,iMid,iHigh);
		return c_1+c_2+c_3;
	}
}


int merge_pairs(int* iDatas,int iLow,int iMid,int iHigh)
{
	int low_length=iMid-iLow+1;
	int high_length=iHigh-iMid;
	int buffer[low_length+high_length];
	int low_index=0;
	int high_index=0;
	int buffer_index=0;
	int n_cross=0;
	while((low_index<low_length)&&(high_index<high_length))
	{
		if(*(iDatas+iLow+low_index)<=*(iDatas+iMid+1+high_index))
		{
			buffer[buffer_index++]=*(iDatas+iLow+low_index++);
		}
		else
		{
			//计数
			n_cross+=(low_length-low_index);
            buffer[buffer_index++]=*(iDatas+iMid+1+high_index++);
		}
	}
	if(low_index==low_length)
	{
		while(high_index<high_length)
		{
			buffer[buffer_index++]=*(iDatas+iMid+1+high_index++);
		}
	}
	else
	{
		while(low_index<low_length)
		{
			buffer[buffer_index++]=*(iDatas+iLow+low_index++);
		}
	}
	for(int i=0;i<buffer_index;++i)
	{
		*(iDatas+iLow+i)=buffer[i];
	}
	return n_cross;
}


int main()
{
	int n_length;
	int buffer[1024];
	printf("pleas input the length : ");
	scanf("%d",&n_length);
	for(int i=0;i<n_length;++i)
	{
		printf("\ninput the %d data : ",i+1);
		scanf("%d",(buffer+i));
	}
	printf("\b result is :  %d ",reverse_order_pairs(buffer,0,n_length-1));
	return 0;
}
