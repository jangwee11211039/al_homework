/*************************************************************************
	> File Name: homework_9.c
	> Author: Jangwee
	> Mail: jangwee11211039@gmail.com 
	> Created Time: 2014年03月31日 星期一 12时00分26秒
 ************************************************************************/

#include<stdio.h>

void quick_sort(int*,int,int);
void swap_(int* ,int*);

void quick_sort(int* iDatas,int iLow,int iHigh)
{
	if(iLow<iHigh)
	{
		int mid_index=0;// less tail
		int pos_index=1;
		while((iLow+pos_index)<=iHigh)
		{
			if(*(iDatas+iLow+pos_index)<*(iDatas+iLow))
			{
				++mid_index;
				if(pos_index!=mid_index)
				{
					swap_(iDatas+iLow+pos_index,iDatas+iLow+mid_index);
				}
			}
			++pos_index;
		}
		swap_(iDatas+iLow,iDatas+iLow+mid_index);
		quick_sort(iDatas,iLow,iLow+mid_index-1);
		quick_sort(iDatas,iLow+mid_index+1,iHigh);
	}
}

void swap_(int* sw1,int* sw2)
{
	int temp=*sw1;
	*sw1=*sw2;
	*sw2=temp;
}


int main()
{
	int n_length;
	int buffer[1024];
	printf("please input the length of array: ");
	scanf("%d",&n_length);
	for(int i=0;i<n_length;++i)
	{
		printf("\n input the %d number: ",i+1);
		scanf("%d",buffer+i);
	}
	quick_sort(buffer,0,n_length-1);
	for(int i=0;i<n_length;++i)
	{
		printf("%d  ",*(buffer+i));
	}
	return 0;
}
