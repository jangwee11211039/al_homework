/*************************************************************************
	> File Name: homework4.c  Fibonacci_Problem
	> Author: Jangwee
	> Mail: jangwee11211039@gmail.com 
	> Created Time: 2014年03月30日 星期日 13时25分06秒
 ************************************************************************/

#include<stdio.h>

int fibonacci(int n)
{
	if(1==n)
		return 2;
	if(2==n)
		return 4;
	else 
		return fibonacci(n-1)+fibonacci(n-2);
}

int main()
{
	printf("please input the mouth later:");
	int n;
	scanf("%d",&n);
	printf("the number of rabbits is :%d\n",fibonacci(n));
	return 0;
}
