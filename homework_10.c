/*************************************************************************
	> File Name: homework_10.c  随机化快排
	> Author: Jangwee
	> Mail: jangwee11211039@gmail.com 
	> Created Time: 2014年03月31日 星期一 14时27分20秒
 ************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int srand_partition(int*,int,int);
int swap_(int*,int*);
void quick_sort(int*,int,int);

int srand_partition(int* iDatas,int iLow,int iHigh)
{
	int mid_index=-1;
	int pos_index=0;
	while((iLow+pos_index)<=iHigh)
	{
		if(*(iDatas+iLow+pos_index)<*(iDatas+iLow))
		{
			++mid_index;
			if(pos_index!=mid_index)
				swap_(iDatas+iLow+pos_index,iDatas+iLow+mid_index);
		}
		++pos_index;
	}
	swap_(iDatas+iLow,iDatas+iLow+mid_index);
	return iLow+mid_index;
}

int swap_(int* sw1,int* sw2)
{
	int temp=*sw1;
	*sw1=*sw2;
	*sw2=temp;
}

void quick_sort(int* iDatas,int iLow,int iHigh)
{
	if(iLow<iHigh)
	{
		srand((unsigned)time(NULL));
		int refer=rand()%(iHigh-iLow+1)+iLow;
		swap_(iDatas+refer,iDatas+iLow);
		int mid=srand_partition(iDatas,iLow,iHigh);
		quick_sort(iDatas,iLow,mid-1);
		quick_sort(iDatas,mid+1,iHigh);
	}
}


int main()
{
	int n_length;
	int buffer[1024];
	printf("please input the length of array: ");
	scanf("%d",&n_length);
	for(int i=0;i<n_length;++i)
	{
		printf("\n input the %d number: ",i+1);
		scanf("%d",buffer+i);
	}
	quick_sort(buffer,0,n_length-1);
	for(int i=0;i<n_length;++i)
	{
		printf("%d  ",*(buffer+i));
	}
	return 0;
}
