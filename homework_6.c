/*************************************************************************
	> File Name: homework_6.c
	> Author: Jangwee    整数划分
	> Mail: jangwee11211039@gmail.com 
	> Created Time: 2014年03月30日 星期日 15时35分20秒
 ************************************************************************/

#include<stdio.h>

int integer_divide(int eles,int max_divide)
{
	if(1==eles)
		return 1;
	if(max_divide<=1)
		return 1;
	if(max_divide>eles)
		return integer_divide(eles,eles);
	return integer_divide(eles-max_divide,max_divide)+ \
		   integer_divide(eles,max_divide-1);
}

int main()
{
	int eles_;
	printf("\nthe integer to divide :");
	scanf("%d",&eles_);
	printf("\n the number of sorts :%d\n",integer_divide(eles_,eles_));
	return 0;
}


