/*************************************************************************
	> File Name: homework_12.c DP 最小矩阵计算问题
	> Author: Jangwee
	> Mail: jangwee11211039@gmail.com 
	> Created Time: 2014年04月05日 星期六 15时33分50秒
 ************************************************************************/

#include<stdio.h>
#define LARGE 1E5


inline int min_(int ref,int w)
{
	return (ref>w?w:ref);
}


int min_matrix_cal(int* matrix,int length)
{
	int note_table[length][length];
	int i,j,len,m,ref;

	for(len=0;len<length;++len)
	{
		for(i=0;i<length;++i)
		{
			j=i+len;
			if(j==i)
			{
				note_table[i][j]=0;
				continue;
			}
			if(j>length)
			{
				break;
			}
			ref=LARGE;
			for(m=i;m<j;++m)
			{
				ref=min_(ref,note_table[i][m]+note_table[m+1][j]+ \
						    (*(matrix+i))*(*(matrix+m+1))*(*(matrix+j+1)));
			}
			note_table[i][j]=ref;
		}
	}
	for(i=0;i<length;++i)
	{
		printf("\n");
		for(j=0;j<length;++j)
		{
			printf("%d  ",note_table[i][j]);
		}
	}
		
	return note_table[0][length-1];
}



int main()
{
	int line[7]={30,35,15,5,10,20,25};
	int length=6;
	printf("result : %d\n",min_matrix_cal(line,length));
	return 0;
}
