/*************************************************************************
	> File Name: homework_3.c  Hanoi_Problem
	> Author: Jangwee
	> Mail: jangwee11211039@gmail.com 
	> Created Time: 2014年03月30日 星期日 12时51分07秒
 ************************************************************************/

#include<stdio.h>



void hanoi(int n,char source,char dest,char midd)
{
	if(0==n)
		return;
	hanoi(n-1,source,midd,dest);
	//move
	printf("move:index_ %d from %c to %c \n ",n,source,dest);
	hanoi(n-1,midd,dest,source);
}

int main()
{

	int n;
	printf("please input the height of hanoi \n");
	scanf("%d",&n);
	char a='a';
	char b='b';
	char c='c';
	hanoi(n,b,c,a);
	return 0;
}
