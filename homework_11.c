/*************************************************************************
	> File Name: homework_11.c  DP(钢条切割)
	> Author: Jangwee
	> Mail: jangwee11211039@gmail.com 
	> Created Time: 2014年04月02日 星期三 13时15分03秒
 ************************************************************************/

#include<stdio.h>

int price_table[10]=
{
	1,5,8,9,10,17,17,20,24,30
};

inline int max(int a,int b)
{
	return (a>b?a:b);
}

int profit_best(int length)
{
	int profit_table[length+1];
	profit_table[0]=0;
	int q=-1;
	for(int len=1;len<length+1;++len)
	{
		q=-1;
		for(int i=1;i<len+1;++i)
		{
			q=max(q,price_table[i-1]+profit_table[len-i]);
		}
		profit_table[len]=q;
	}
	return profit_table[length];
}


int main()
{
	int length;
	printf("please input the length of steel bar: ");
	scanf("%d",&length);
	printf("the result : %d ",profit_best(length));
	return 0;
}


