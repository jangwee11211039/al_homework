/*************************************************************************
	> File Name: homework_7.c 归并排序
	> Author: Jangwee
	> Mail: jangwee11211039@gmail.com 
	> Created Time: 2014年03月30日 星期日 16时19分21秒
 ************************************************************************/

#include<stdio.h>

void merge_sort(int*,int,int);
void merge(int*,int,int,int);

void merge_sort(int* iDatas,int iLow,int iHigh)
{
	if(iLow<iHigh)
	{
		int iMid=(iLow+iHigh)/2;
		merge_sort(iDatas,iLow,iMid);
		merge_sort(iDatas,iMid+1,iHigh);
		merge(iDatas,iLow,iMid,iHigh);
	}
}

void merge(int* iDatas,int iLow,int iMid,int iHigh)
{
	int low_length=iMid-iLow+1;
	int high_length=iHigh-iMid;
	int iBuffer[low_length+high_length];
	int low_index=0;
	int high_index=0;
	int buffer_index=0;
	while((low_index<low_length)&&(high_index<high_length))
	{
		if(*(iDatas+iLow+low_index)>*(iDatas+iMid+1+high_index))
		{
			iBuffer[buffer_index]=*(iDatas+iMid+1+high_index);
			++high_index;
		}
		else
		{
			iBuffer[buffer_index]=*(iDatas+iLow+low_index);
			++low_index;
		}
		++buffer_index;
	}
	if(low_index==low_length)
	{
		for(int i=0;i<(high_length-high_index);++i)
		{
			iBuffer[buffer_index++]=*(iDatas+iMid+1+high_index+i);
		}
	}
	else
	{
		for(int i=0;i<(low_length-low_index);++i)
		{
			iBuffer[buffer_index++]=*(iDatas+iLow+low_index+i);
		}
	}

	for(int i=0;i<buffer_index;++i)
	{
		*(iDatas+iLow+i)=iBuffer[i];
	}		
}

int main()
{
	int length;
	int num_array[1024];
	printf("\nplease input the length of array: ");
	scanf("%d",&length);
	for(int i=0;i<length;++i)
	{
		printf("\n please input the %d number : ",i+1);
		scanf("%d",num_array+i);
	}
	merge_sort(num_array,0,length-1);
	for(int i=0;i<length;++i)
	{
		printf("%d  ",*(num_array+i));
	}
	return 0;
}
