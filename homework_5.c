/*************************************************************************
	> File Name: homework_5.c stirling_porblem
	> Author: Jangwee
	> Mail: jangwee11211039@gmail.com 
	> Created Time: 2014年03月30日 星期日 13时46分59秒
 ************************************************************************/

#include<stdio.h>

int stirling(int num_elements,int num_sets)
{
	if(num_elements==0)
		return 0;
	if(num_sets>num_elements)
		return 0;
	if(num_elements==num_sets)
		return 1;
	if(1==num_sets)
		return 1;
	else
		return num_elements*stirling(num_elements-1,num_sets)+ \
			   stirling(num_elements-1,num_sets-1);
}

int main()
{
	int n_eles;
	int n_sets;
	printf("\nplease input the number of elements: ");
	scanf("%d",&n_eles);
	printf("\nplease input the number of sets: ");
	scanf("%d",&n_sets);
	printf("\nthe result of : %d",stirling(n_eles,n_sets));
	return 0;
}
